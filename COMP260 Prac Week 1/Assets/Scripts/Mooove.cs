﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mooove : MonoBehaviour
{

    #region Public variables
    public Vector3 startPoint;
    public Vector3 endPoint;
    public float speed = 1.0f;

    #endregion

    #region Private Variables
    public bool isMovingForward = true;


    #endregion

    // Use this for initialization
    void Start()
    {
        //Snap to start point
        transform.position = startPoint;
    }

    // Update is called once per frame
    void Update()
    {
        //make target exist
        Vector3 target;

        ///Check direction
        if (isMovingForward)
        {
            target = endPoint;
        }
        else
        {
            target = startPoint;
        }

        //Calc distance to move -- judge speed & Frame rate
        float distanceToMove = speed * Time.deltaTime;
        Debug.Log("Distance to move =" + distanceToMove);

        //Check dist to target
        float distanceToTarget = (target - transform.position).magnitude;
        Debug.Log("Distance to target =" + distanceToTarget);


        ///Check if we are "in range"

        if (distanceToMove > distanceToTarget)
        {
            //this is true when close.
            //Therefore, snap to position/war[ and change direction
            transform.position = target;

            //Change bool to false
            isMovingForward = !isMovingForward;
            //Debug.Break(); /* Pause the game*/
        }
        else
        {
            //If we're not in range, keep moving
            Vector3 direction = (target - transform.position).normalized;
            transform.position += direction * distanceToMove;
        }
    }
}
